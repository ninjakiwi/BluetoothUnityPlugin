package com.bike.team.btuplugin;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import com.unity3d.player.UnityPlayer;


public class Bluetooth {
    private final String TAG = "TAG";
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private static Bluetooth instance;
    private Context context;
    private ConnectedThread mConnectedThread;

    /** Constructor checks that the device is paired to the arduino
     * Then makes a connection
     * Finally starts the ConnectedThread
     */

    public Bluetooth() {
        this.instance = this;

        Log.d(TAG, "Bluetooth: start");
        //String bti = BTinit();

        if (BTinit()) {
            Log.d(TAG, "Bluetooth: BTinit Success");
            //if (BTconnect()) {
                run();
                Log.d(TAG, "Bluetooth: BTconnect Success, Connection opened");
            //} else {
            //    UnityPlayer.UnitySendMessage("Player", "fail", "BTconnect");
            //}

        } else {
            UnityPlayer.UnitySendMessage("Player", "fail", "BTinit");
            //Toast.makeText(this.context, "BT can't init", Toast.LENGTH_SHORT).show(); // this.context is not set
        }
        //UnityPlayer.UnitySendMessage("Player", "move", "BT");
    }

    public static Bluetooth instance() {
        if (instance == null) {
            instance = new Bluetooth();
        }
        return instance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void showMessage(String msg) {
        boolean conn = (mConnectedThread != null);
        Toast.makeText(this.context, "Connected: " + conn + " Msg: " + msg, Toast.LENGTH_SHORT).show();
    }

    public void run(){
        mConnectedThread = new ConnectedThread(socket);         //constructs a ConnectedThread
        mConnectedThread.start();                                               //Starts the ConnectedThread run() method
    }

    private boolean BTinit() {
        Log.d(TAG, "BTinit: start");

        //final String DEVICE_ADDRESS = "98:D3:81:F9:65:A3"; // Arduino
        //final String DEVICE_ADDRESS = "08:00:00:68:C3:68"; // Tablet
        //final String DEVICE_ADDRESS = "A8:81:95:1B:53:25"; // Daniel's phone

        // Auto select
        final String[] device_addresses = new String[] {"98:D3:81:F9:65:A3","A8:81:95:1B:53:25"};

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Log.d(TAG, "BTinit: btAdapter fail, device doesn't support bt");
            //showMessage("BTinit: btAdapter fail, device doesn't support bt");
            return false;
        }
        try {
            Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
            if (bondedDevices.isEmpty()) {
                Log.d(TAG, "BTinit: No bonded devices");
//                return "BTinit: No bonded devices";
                return false;
                //showMessage("BTinit: No bonded devices");
            } else {
                for (String address : device_addresses) {
                    for (BluetoothDevice iterator : bondedDevices) {
                        Log.d(TAG, "BTinit: Itterator address: " + iterator.toString() + "Device address: " + address);
                        if (iterator.getAddress().equals(address)) {
                            device = iterator;
                            Log.d(TAG, "BTinit: device " + address + "bonded");
                            //return iterator.getAddress();
                            if (BTconnect())
                                return true;
                        }
                    }
                }
            }
        }
        catch (NullPointerException e){
            Log.d(TAG, "BTinit: No paired devices, " + e.getMessage());
//            return "BTinit: No paired devices";
            return false;
        }
        return false;
    }

    private boolean BTconnect() {
        Log.d(TAG, "BTconnect: start");
        boolean connected = true;
        BluetoothSocket tmp = null;

        try {
            tmp = device.createInsecureRfcommSocketToServiceRecord(PORT_UUID);
            Log.d(TAG, "BTconnect: rfcommsocket success");
        } catch (IOException e) {
            Log.d(TAG, "BTconnect: rfcommsocket fail");
        }
        socket = tmp;

        try {
            socket.connect();
            Log.d(TAG, "BTconnect: socket connect success");
        } catch (IOException e) {
            Log.d(TAG, "BTconnect: socket connect fail " + e.getMessage());
            try {
                socket.close();
            } catch (IOException e1) {
                Log.d(TAG, "BTconnect: " + e1.getMessage());
            }
            connected = false;
        }
        catch (NullPointerException e){
            Log.d(TAG, "BTconnect: not paired, " + e.getMessage());
        }
//        if (connected) {
//            try {
//                InputStream inputStream = socket.getInputStream();
//                Log.d(TAG, "BTconnect: InputStream received");
//            } catch (IOException e) {
//                e.printStackTrace();
//                Log.d(TAG, "BTconnect: InputStream not received " + e.toString());
//            }
//        }
        return connected;
    }
}
