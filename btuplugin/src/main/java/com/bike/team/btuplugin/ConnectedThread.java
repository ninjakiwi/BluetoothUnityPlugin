package com.bike.team.btuplugin;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.unity3d.player.UnityPlayer;

import java.io.IOException;
import java.io.InputStream;

public class ConnectedThread extends Thread {
    private final InputStream mmInStream;
    private static final String TAG = "ConnectedThread";
    private Handler mHandler;

    public ConnectedThread(BluetoothSocket socket) {
        Log.d(TAG, "Init fn: Starting.");
        InputStream tmpIn = null;
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mmInStream = tmpIn;

        mHandler = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message inputMessage) {
                //do stuff
                UnityPlayer.UnitySendMessage("Player", "move", "FAST");
                //showMessage("Hi");
            }
        };
    }

    public void run() {
        byte[] buffer = new byte[1024];  // buffer store for the stream
        int bytes; // bytes returned from read()



        // We will need to sit in this method for the whole program unless an error occurs
        while (true) {
            // Read from the InputStream
            try {
                bytes = mmInStream.read(buffer);
                final String incomingMessage = new String(buffer, 0, bytes);

                // Send the obtained bytes to the UI activity.
                Message readMsg = mHandler.obtainMessage(
                    0, bytes, -1, buffer);
                readMsg.sendToTarget();

                Log.d(TAG, "run: Got input");
            } catch (IOException e) {
                Log.e(TAG, "run: Error reading Input Stream. " + e.getMessage());
                break;
            }
        }
    }
}
